/**Caleb Honeycutt

 * 
 * This is homework assignment 1, it is designed to generate 100 random numbers and  organize
 * them into a chart of 10 different categories.
 * 
 */

//imports java util to use the random class 
import java.util.*;

public class Driver {
	
	
	public static void main (String[] args) {
	int chance;
	int length = 0;
	int maxLength = 0;
	int totRemoved = 0;
	//creates the queue
	Queue list = new Queue();
	//random number generator
	Random randomGen = new Random();
	
	for (int i = 0; i < 60; i++)//60 minutes of activity
	{
		if (length > maxLength){//finds current max length
			maxLength = length;
		}
		
		chance = randomGen.nextInt(100) + 1;	
		if (chance >= 0 && chance <= 25)//25% chance customer is added using Rand
		{
			list.enqueue(new Customer());
			length++;
			System.out.println("New customer added!  Queue length is now: " + length);			
		}	
		
		if(list.isEmpty() == false){
			list.first.decServiceTime();//dec service time for the interation
			System.out.println(list.first.getServiceTime());
		if (list.first.getServiceTime() == 0)//Did decServiceTime go to zero?
		{
			length--;
			list.dequeue();
			System.out.println("Customer serviced and removed from the queue.  Quest length is now " + length);
			totRemoved++;
		}
		}
		
		
		System.out.println("---------------------------------------------------");
	}
	System.out.println("End of program Information:");
	System.out.println("Total Customers served: " + totRemoved);
	System.out.println("Maximum line length during the simulation: " + maxLength);
	}
}
	
 	
			
	