
public class Queue {
	public Customer last, first;

	public Queue() {
		first = null;
		last = null;
	}

	public boolean isEmpty() {
		return first == null;
	}

	public void enqueue(Customer add) {
		if (!isEmpty()) {
			last.setNext(add);
			last = add;
			System.out.println("Customer added");
		}
		else if (isEmpty()){
			first = last = add;
		}
	}
	
	public void dequeue(){		
		first = first.getNext();							
		}		
	}
	

